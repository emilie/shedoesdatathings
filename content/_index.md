+++

+++
I am increasingly struck by the idea that technical blogs are just a way of building your own technical documentation. You write about the problem you had and how you solved it, in part to helps others who might stumble upon, but even more so to help yourself if you find yourself in that predicament again.

[Meet Emilie.](http://emilieschario.com)