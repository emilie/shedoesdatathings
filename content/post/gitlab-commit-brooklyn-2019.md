---
title: GitLab Commit Brooklyn 2019
date: 2019-09-18T04:00:00.000+00:00
tags: ["gitlab commit", "conference"]
photo: "/uploads/gitlab_commit_brooklyn_2019_graffiti.jpg"
include_toc: false
---
<center>
![](/uploads/gitlab_commit_brooklyn_2019_graffiti.jpg)
</center>

Yesterday, I spent the day savoring GitLab's inaugural user conference GitLab Commit in Brooklyn, and it was so great. I'm already looking forward to the next one!

***

The event started for me on Monday night at the Speaker's Dinner because I would be participating in one panel and delivering a lightning talk the next day.

<center>
![](/uploads/gitlab_commit_brooklyn_2019_speakers_welcome.JPG)
</center>

After cocktail hour, the dinner began with opening remarks from Sid.

<center>
![](/uploads/gitlab_commit_brooklyn_2019_speakers_dinner_menu.jpg)
</center>

The menu did not disappoint- no surprise since the event was at the [Bouley Test Kitchen](https://davidbouley.com/bouley-test-kitchen/). I pushed myself to get to know customers and partners in attendance and hear about their experiences using GitLab.

<center>
![](/uploads/gitlab_commit_brooklyn_2019_speakers_dinner_food.JPG)
</center>

The menu did not disappoint!

The next morning started bright and early with registration and coffee.

<center>
![](/uploads/gitlab_commit_brooklyn_2019_donuts.jpg)
</center>

AND DONUTS!

The great thing about being at Commit is that there were no details left unattended. You know what you were there for- down to the napkins that you got with your coffee.

<center>
![](/uploads/gitlab_commit_brooklyn_2019_fundraising_announcement.jpg)
</center>

During the Opening keynote, CEO Sid got to announce our Series E Funding: $268 million raised to invest in growth, valuing the company at $2.75 billion. Incredible. [The Forbes coverage](https://www.forbes.com/sites/alexkonrad/2019/09/17/gitlab-doubles-valuation-to-nearly-3-billion/) did, in my opinion, a great job.

<center>
![](/uploads/gitlab_commit_brooklyn_2019_heroes.jpg)
</center>

The expo hall included booths, highlighting the product direction, the UX team, our partners, and some GitLab security initiatives. I loved meeting some of our [GitLab Heroes](https://about.gitlab.com/community/heroes/) in person.

<center>
![](/uploads/gitlab_commit_brooklyn_2019_street.jpg)
</center>

Afternoon sessions were located across buildings, and no feature was missed to make you feel like you were in the heart of GitLab the whole time. The sidewalk guided you to where you were going, in addition to staff being there to help point you in the right direction.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The panel I am most excited about: DataOps in a Cloud Native world <a href="https://twitter.com/hashtag/GitLabCommit?src=hash&ref_src=twsrc%5Etfw">#GitLabCommit</a> <a href="https://t.co/UaCdJsPL2f">pic.twitter.com/UaCdJsPL2f</a></p>— Amruta Ranade (@AmrutaRanade) <a href="https://twitter.com/AmrutaRanade/status/1174032648769024006?ref_src=twsrc%5Etfw">September 17, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

In the first part of the afternoon, we had a panel on DataOps. It was great to share the stage with [Claire](https://twitter.com/clairebcarroll), [Danielle](https://twitter.com/DanielleMorrill), Priyanjna, and [Priyanka](https://twitter.com/pritianka).

<center>
![](/uploads/gitlab_commit_brooklyn_2019_bikes.jpg)
</center>

Every detail was taken care of!

<center>
![](/uploads/gitlab_commit_brooklyn_2019_lightning_talk.jpg)
</center>

I got the lucky pleasure of speaking in the last speaker slot of the day presenting on _How to deploy dbt using GitLab CI_- more on that soon! (Thanks [Claire](https://twitter.com/clairebcarroll) for snapping this picture!)

<center>
![](/uploads/gitlab_commit_brooklyn_2019.jpg)
</center>

We wrapped up an incredible day with bowling! I am "FKA (Formerly Known As) Data Eng" ([Twitter thread](https://twitter.com/emilieschario/status/1171041370326347777)). This was my every-couple-months-reminder that I am terrible at bowling.

GitLab Commit was an incredible event. The day was full of learning, hearing about the great technical work that other people are doing, and connecting with new folks. If you have the opportunity to attend, you definitely should!