---
title: How adding a template to my calendar keeps me productive
date: 2021-08-02T04:00:00.000+00:00
tags: []
photo: "/uploads/calendar_template.png"

---
When I decided to go back to school while pregnant, I heard a lot of the “school, work, life- pick two” trope. And, unsurprisingly to anyone who has known me, I was determined to prove them wrong. Nearly a year in, here’s a bit of detail on how it works.

![](/uploads/calendar_template.png)

This is the template for my calendar.

There's a couple of structural guidelines that this provides to my day-

I am the primary parent for RJ in the mornings before school. When he wakes up, I give him his morning bottle and get him (and his stuff) ready for daycare. Monday through Friday, I get him ready, we hang out, and then take him to daycare.

After dropping him off at daycare, I go to the gym where I study for an hour before my workout starts at 8:30. This focused hour of school work away from my desk allows me to do my reading while I'm away from home. My goal is to go to the gym 4 times per week ("more often than I don't").

After the gym, I come home to work, where I work for the rest of the day. I don't usually take a dedicated lunch.

While I don't usually do daycare pickup when C is home, in case I have to, I always have that time blocked off. This is non-negotiable.

When he gets home, I usually take that as an opportunity to wrap up my day and transition into other things.

My evenings are dependent on my class schedule for that quarter. I currently have sync classes on Monday and Wednesday. (Next quarter, it'll probably be Monday, Tuesday, and Wednesday.). The other nights are available for work-adjacent things, catching up on work, and doing more school work.

## Batching

There are several work-adjacent things that have crept up in my calendar over time- mentorship calls, mentoring calls, intros, and more. I've focused on concentrating these into the yellow blocks- Tuesday focused on Europe and the US, Thursday focused on the US, and a Tuesday evening for later West Coast/APAC, as needed. By consolidating or _batching_ these, I'm more productive and more prepared. It also helps cap how many of these calls I can do per week.

I apply a similar batching principle to my 1:1s with my reports- doing one group on Wednesday afternoon for US-based (mostly PT) team members and one group on Thursday mornings with EMEA and US team members. This means that I can block out an hour or two for 1:1 prep on Tuesday and another on Wednesday and walk into my 1:1s prepared.

## Saturdays

I don't work on Saturdays. I don't do personal work. I don't do school work. I don't do work. Work expands to fill all available space and I do not let it permeate Saturdays.

There's a caveat here that this isn't always true. I had a midterm last week and I studied on Saturday, but I don't _plan_ things for Saturday.

## But, it's just a template

This isn't perfect. Not every week looks like this. RJ is taking [ISR Self-Rescue](https://www.infantswim.com) classes right now, so I haven't made it to CrossFit for a bit. This, though, is the starting point. It gives me [Simple Rules](https://hbr.org/2012/09/simple-rules-for-a-complex-world) that can help guide when and how I book obligations, manage my team, and get _all_ the things done.