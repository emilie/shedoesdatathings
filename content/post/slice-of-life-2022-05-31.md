---
title: Slice of life 2022-05-31
date: 2022-06-01T04:00:00.000+00:00
tags: []
photo: ''

---
Over the last two years, I’ve heard “I don’t know how you do it” so many times. This phase of life of parenting/working/going to school (remember that my husband was gone abroad for a couple of months there too) has been hard. And, in many ways, for many reasons, I am grateful to see it come to a close.

But I’ve been thinking about something a coworker said a couple of weeks ago: “Wait until \~2 months from now when you’re trying to figure out how you balanced this as you'll notice you still have zero free time.” I’m sure that’s true (especially once I enter the newborn phase again this fall).

But, for right now, I want to capture it- I don’t want to remember, I want to know. I want to look back and remind myself that I wanted it this bad and that I was willing to work this hard.

Here’s what yesterday looked like:

* 3 AM Wake up.- My mom (who was visiting) was catching a Lyft to the airport shuttle and I wanted to be back up in case the ride didn’t come. It did.
* 3:30 AM: Realize I’m not going back to sleep and get up; make coffee.
* 4 AM: Homework. -Start reviewing the materials for my technology strategy and innovation class.
* 5 AM: My husband is leaving, we chat briefly, but mostly go about our mornings separately with me doing homework.
* 6:30 AM: Realize RJ has to be waking up soon. Prep his daycare bag, swim bag, and morning bottle.
* 6:45 AM: Wake RJ up and give him his morning bottle. Get ready for swim class. RJ says good morning to Bo
* 7:20 AM: Lakiah picks up RJ and takes him to swim; I go back to homework.
* 8 AM: Finish review of all the async materials and reading the case that I have to write a paper for due at 8 PM. Decide to nap for 40 minutes, set alarm and back up alarm.
* 8:45 AM: Wake up. Ugh.
* 9 AM: First work meeting. I am meeting until 10:30
* 10:30 AM: Go shower
* 11 AM: Meetings all the way until 6 pm. Between meetings, I eat, write a paper due at 8 pm, put together my Coalesce proposal, and write three intro emails.
* 5:30 PM: During this call, I make dinner which is meatloaf and salad.
* 6 PM: Daycare pickup; Catch up phone call with best friend
* 6:30 PM: Return home and feed RJ (meatballs with beets); eat dinner. Casey comes home.
* 6:55 PM: Kiss RJ good night. Go to class.
* 7 PM: Sustainability and strategy class until 8:30 PM.
* 8:30 PM: Meet with a group for Innovation in Developing Countries project until 8:45.
* 8:45 PM: Get water and a snack. Say good night to my husband who will be in bed when I’m out of class.
* 9 PM: Innovation in Developing Countries until 10:30 PM
* 10:30 PM: Finish class; recognize I’m too awake to go right to bed. Try to chill for a little while.
* 11 PM: Go to bed.

Not all days are like this. But this one was long.

No reflection, no thoughts, not some self-aggrandizing "if I can do it, you can do it too." Some days are hard. But I just don't want to forget.