---
title: Send Freewrite posts write to WordPress
date: 2020-04-16T04:00:00+00:00
tags: []
photo: "/uploads/zap_overview.jpg"
include_toc: false
---
I have loved my [Freewrite](https://getfreewrite.com/) since I got it two years ago. The machine in itself is a bit bulky, and that bulk can make it a bit difficult to use. For example, it can never travel with me. I know that writing is a muscle I should be exercising on a regular basis, so I want to make it easier to use.

I'm not going to change the bulk of the machine (though the [Traveler](https://getfreewrite.com/products/freewrite-traveler) will help with that!), but what if I could solve some of the other pain points associated with using my Freewrite?

One of my biggest pain points is that I'd write things and they'd be saved to my Postbox account/emailed to me, but I still had to copy and paste them into WordPress. Either I logged into Postbox and copy and pasted or, more often, I took the txt file that had been emailed to me and copy and pasted that into WordPress. That meant navigating email (a distraction) while trying to get a post out the door.

I just set up a simple Zapier integration that helps address this: Set up a WordPress blog post from emails sent to a specific Zapier email.

![](/uploads/zap_overview.jpg)

This was pretty simple: When an email goes to a specific Zapier email address, set up a post in Wordpress.

For the trigger:

![](/uploads/zap_inbound_email.jpg)

When I send an email to this specific email address, the trigger is activated.

For the action:

![](/uploads/zap_create_post.jpg)

We create a WordPress post. I use the subject line of the email as the title of the post. The `.txt` attachment becomes a URL in the body of the post itself. I'm set as the author, and the post is saved as a draft.

This means what when I log into WordPress, everything is there for me to review, add images, and publish.

The last piece: I had to login to Postbox and [update the email address associated with my account](https://support.getfreewrite.com/article/39-how-to-change-postbox-email-address-or-username).

Just one less step to worry about now.