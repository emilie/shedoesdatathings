---
title: People Handoffs
date: 2022-07-07T04:00:00+00:00
tags:
- connecting people
photo: ''

---
It was October 2021 and I was on my first business trip in about 18 months. I was on my first anything-trip in about 18 months. In those 18 months since the COVID pandemic had started, so much about my life had changed. Before March 2020, I had been on the road for work about once a month. Now in October 2021, I was two jobs, one baby, and a whole deployment later trying to navigate what a “new normal” looked like for our family of three humans and one four-legged companion.

I hadn’t made much small talk in that 18-month window and had come to like it that way. Leading a team, my focus was always on my reports, so our conversation tended to focus on them instead of simple banter. I didn’t thrive or enjoy the chit-chat that I had gotten so good at avoiding. My schtick was (and still is) starting and ending meetings on time in a highly unfashionable but also endearing way.

So, October 2021, I’m sitting at dinner in this group of nearly 20 people. And, I’m out of conversation topics. I’m done. My brain is mush; I don’t know these people. Why do we still have two hours to go? Why is dinner at 9 PM?

***

As a child, I remember my mom talking about her boss Kevin having read one of those books about making conversation. When I asked her what she meant by that, she mentioned that Kevin could always keep a conversation going. He’d ask you about your life and your kids. The next time you’d meet him, he wouldn’t remember that you’d met and he’d pull out the same playbook. You could have the same conversation with him tons of times and he would have no idea.

I always thought that was kinda gross. I’d rather not have a playbook and just end the conversation.

***

In college, I was that person who liked to do laps at a party. It didn’t matter who I went out with- my team, my sorority sister, my then-boyfriend (now-husband). I’m just one of those people who enjoyed bouncing around, making sure things are moving, always going somewhere. I loved seeing the variety of people there. At CDE, I’d much rather enjoy the 50 people I stumbled upon and had quick chats with over fewer deep heart-pouring conversations.

I realized recently that this tendency is still very real. At Data Council, I planted myself on a comfy couch in the hall and I let people come and go. Over the course of the last day, I felt like I talked with tons of people at a variety of depths. I certainly didn’t have any serious heart-to-hearts, but I did have many conversations that planted the seed for other, future conversations.

I am still thinking about many of these conversations.

***

I remember during sorority recruitment learning about handoffs of people. When it was time to pass off the conversation to the next sister so that you could get to know the next person, someone would come take over the conversation and you’d hand it off.

Jenny walks up- “Hey Jenny, Have you met Leah? We were just talking about how Leah is thinking about going to law school after graduation. Leah, Jenny is on the volleyball team.” After a sentence or two, I would excuse myself so I could continue the chain, interjecting and taking over the conversation from someone else.

There was a clear conversation topic. It was easy to jump into a conversation when a topic was planted and you didn’t have to run through a playbook in your head of how to talk to someone.

***

When we’re at networking events or just events where humans are talking to each other, why don’t we do this? People talk about the importance of standing in a [Pacman circle](https://www.ericholscher.com/blog/2017/aug/2/pacman-rule-conferences/) at conferences, but why don’t we just do more introductions for people. “Do you two know each other? No? Let me introduce you.”

It’s July 2022, and I’ve been on 10 business trips since October 2021 (\~ one per month). I’m still shocked by how often we don’t do handoffs. This habit that I picked up during sorority recruitment nearly a decade ago seems so obvious, so beneficial. Why isn’t it normal?

[We do it via email](https://clrcrl.com/2022/03/05/moving-you-to-bcc.html) but not in person. This seems backward to me.

***

Through the realities of all these nuances of my own person, I’ve come to realize I would prefer to plant seeds that we can sprout separately and turn into something meaningful down the road and, in group settings, I value connecting others much more than I value connecting with others in group settings.