---
title: 'Commit Brooklyn 2019: DataOps in a Cloud Native World at GitLab '
date: 2019-09-27T04:00:00+00:00
tags: []
photo: "/uploads/DataOps in a Cloud Native World.jpg"
include_toc: false
---
I shared last week a bit about the wonderful panel at [GitLab Commit in Brooklyn]() that I was able to participate in.

Now that the video is live, I thought I'd take a second to share the video and a couple of highlights.

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/PLe9sovhtGA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Key takeaways:

* Data is behind Software Development when it comes to learning and implementing the best practices of DevOps. DataOps is the future.
* Places to start: Version Control and CI/CD
* You're fighting years/decades/lifetimes of people working in flows they're interested in (cough.. spreadsheets).
* The number of tools in the data space are making it easier to implement DataOps, [Meltano](https://www.meltano.com/) and [dbt](https://www.getdbt.com/) are just the beginning.
* You can have an all female panel and they don't have to talk about being a woman in tech.

We got some pretty solid coverage from [The New Stack](https://thenewstack.io/gitlab-commit-brooklyn-devops-as-a-single-application/), in case you're interested in hearing a more objective opinion.