---
title: Career paths, data literacy, and a great conversation with Simon Ouderkirk
  and Stephen Levin
date: 2020-04-15T04:00:00+00:00
tags: []
photo: ''
include_toc: false
---
I got the chance to have a really great conversation with Simon Ouderkirk of Automattic (WordPress.com, Jetpack, Tumblr) and Stephen Levin of Zapier earlier this spring about career growth, DataOps, job titles, and more.

Stephen was the perfect pair here, as our careers has both moved from data into cross-functional operations-y roles. Simon is an incredible interviewer. These two were great partners for an awesome conversation.

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/uIEjBVC2roo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Stephen Levin:

* Website: [https://www.stephenlevin.co/](https://www.stephenlevin.co/)
* Twitter: [https://twitter.com/smlevin11](https://twitter.com/smlevin11)

Simon Ouderkirk:

* Website: [https://s12k.com/](https://s12k.com/)
* Twitter: [https://twitter.com/saouderkirk](https://twitter.com/saouderkirk)