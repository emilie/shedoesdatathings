---
title: Year Two Was Different Than What I Expected
date: 2020-07-11T04:00:00+00:00
tags: []
photo: "/uploads/2020-01-14-gitlab-commit-sf.jpg"

---
Just over a year ago, [when I reflected on crossing the 1 year mark at GitLab](http://blog.emilieschario.com/post/1-year-at-gitlab/), that reflection was wholly on my time on the data team. As I looked ahead, I couldn't have imagined anything _but_ a future on the data team.

It's still a lit of a brainmeld, but less than a month after I published that post, on July 19, I'd see an internal opportunity for movement, decide to stay up late one Thursday night updating my resume, and submit an application to a new role outside of the data team all within a 6 hour window. The next day, I'd ping the CEO that I applied and would appreciate his consideration. Five minutes later we'd be on a call. By September 9, I'd start a new role at GitLab as an [Internal Strategy Consultant, Data](https://about.gitlab.com/job-families/chief-executive-officer/internal-strategy-consultant/), reporting directly to the CEO after 15 months on the data team.

![](/uploads/transition_message.png)

From September to March, I'd be Interim [Chief of Staff](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/), while we hired our new Chief of Staff who would be my manager. She would eventually start in March, and I've been reporting to her since then.

One of the highlights of this year was winning one of the [GitLab Values Awards](https://about.gitlab.com/handbook/values/). Every year at our annual get-together called Contribute, we award 6 team members with an award for each of our values- Collaboration; Results; Efficiency; Diversity, Inclusion, and Belonging; Iteration; and Transparency. Contribute was supposed to be in Prague in March but was moved virtual. There I won our Transparency Value Award. [Transparency](https://about.gitlab.com/handbook/values/#transparency) is the hardest value to feel comfortable with, but I've got to truly feel how powerful of a force  it is. I believe in the force of transparency, and I was honored to win.

## Taking Cross Functional to new meaning

I've always loved data as a function because, especially in early-stage data organizations, it is very cross-functional. A day's tasks may have gone from Sales to Product to Marketing but, at the end of the day, I was still doing data work.

Now, my role is cross-functional in a totally new way. It's not enough to just _know_ what was going on in other parts of the business, but I have to be able to jump into other parts of the business and just figure out those skillsets. I might go from writing a feature request to prepping a financial analysis to writing some code to keep the handbook alive to putting out some urgent fire, all in a day's work.

In one sense, I went from data being by job to data being a skill I use to do my job, and my job being whatever the business needs me to focus on at that moment.

Never a boring moment, that's for sure. In fact, I could use a boring moment.

## Vacation and Flexibility

I still took a significant chunk of time off, despite a clear step up in responsibilities at work.

![](/uploads/2019_emilie_in_china.jpg)

At the end of June, I got the chance to go speak at KubeCon Shanghai and took the opportuntiy to visit Shanghai and Beijing. It's still crazy that I got to go to China.

In August, I participated in the [CEO Shadow program](https://about.gitlab.com/handbook/ceo/shadow/), which gave me the opportunity to spend 2 weeks in San Francisco. While there, we made a detour trip to Vancouver for a night and a trip to Vail, Colorado for three. ([I wrote a bit about the program](https://about.gitlab.com/blog/2019/10/07/what-i-learned-about-our-ceo-s-job-from-participating-in-the-ceo-shadow-program/), if you're interested.)

In September, I attended [GitLab Commit Brooklyn](http://blog.emilieschario.com/post/gitlab-commit-brooklyn-2019/), a resounding success, and followed it with a week in Brazil that was spent half working and half on vacation.

![](/uploads/DataOps in a Cloud Native World.jpg "Speaking at GitLab Commit Brooklyn")

When my husband came home from his deployment in October, I took two full weeks of vacation.

In November, I spent half a week in Denver at [Meltano](https://meltano.com) Assemble and a week in SF for our [e-group offsite](https://about.gitlab.com/handbook/ceo/offsite/). I took half a week for the Thanksgiving holiday, which is also our wedding anniversary.

Over the winter holidays, I took 2 weeks off that ended with my husband being unexpectedly deployed.

In January, I went back to SF for [GitLab Commit SF](http://blog.emilieschario.com/post/commit-san-francisco-2020-how-to-implement-dataops-using-gitlab/), which was another incredible event.

![](/uploads/2020-01-14-gitlab-commit-sf.jpg "Speaking at GitLab Commit SF")

At the end of February, I went to Boulder, CO for our [e-group offsite](https://about.gitlab.com/handbook/ceo/offsite/).

In March, when my husband returned from that deployement, I took a week of "available-but-not-working vacation." I was in meetings where I was needed, but I didn't _do_ any work outside of those calls and keeping people unblocked.

And, soon after, the world changed.

I did end up taking one week off in April for a vacation that we had already planned. It was cancelled but we decided to stay-cation anyway. Since then, though, mostly a sprinkled day off here or there.

In case you haven't kept track, that was 7 weeks of **true** vacation over the course of the year. I will never work somewhere that doesn't have an unlimited vacation policy again.

(That, by the way, doesn't count the two weeks my husband and I spent in Bora Bora last June, which delayed me writing last year's reflection. I'm guessing, I counted Bora Bora then.)

## Leadership

A year ago, when I wrote about how much I had learned from watching our leadership ranks, I had no idea what an opportunity for exactly that I was going to have. In this role, I've gotten to run 3 OKR cycles, organized 3 Quarterly E-Group Offsites (one all-online thanks to the 'Rona), run a Board Meeting, and learned tons about financial planning, balancing conflicting priorities, talent management, and so many other subjects that I'd otherwise never have had access to.

I could read as many business book as I'd like, but nothing would give me the crash course I got this year. It. was. awesome.

When I joined GitLab 2+ years ago, I was employee number 290. Today, we are over 1300. **I joined GitLab over a thousand people ago**. In a lot of ways, it's almost a different company. That being said, I know I still have a lot to learn.

So, what's next?

<center>
  <iframe width="560" height="315" src="https://www.youtube.com/embed/gSUHgAp7V7c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>