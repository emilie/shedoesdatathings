---
title: 'Commit Brooklyn 2019: Deploying your first dbt project with GitLab CI'
date: 2019-09-30T04:00:00+00:00
tags: []
photo: "/uploads/Deploying your first dbt project with GitLab CI.jpg"
include_toc: false
---
Just sharing this video on deploying your first dbt project with GitLab CI

<center> <iframe width="560" height="315" src="https://www.youtube.com/embed/-XBIIY2pFpc?start=1306" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>