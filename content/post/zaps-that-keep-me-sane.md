---
title: Zaps that keep me sane
date: 2021-09-23T04:00:00+00:00
tags: []
photo: ''

---
Following up on [How adding a template to my calendar keeps me productive](http://blog.emilieschario.com/post/how-adding-a-template-to-my-calendar-keeps-me-productive/), I thought I'd share how I use a number of Zaps (on Zapier) to also keep my productive. Automation is key to how I get things done. 

A couple of things: 

* I pay for Zapier. 
* I have a paid Slack account (that's just mine). 

It's worth paying for things that produce ROI. The efficiency I gain from these Zaps (and then many things I do with that paid Slack account) more than pay for themselves. Paying for good software is a great thing!

## Share Twitter Mentions in Slack

[Zap link](https://zapier.com/shared/797425def30c01d16bea9f074a95c3ec91252c07)

I'm in this bad habit of checking twitter and not replying to things I should reply to. I check this channel a couple of times a week and emoji react to things I'm "done" with. (I use a similar method of managing GitHub mentions.) 

## dbt & LO YouTube channels to Todoist

[dbt YouTube Zap link](https://zapier.com/shared/88a75af653a05cd4acf494b896ee457a2581bb5e)

[LO YouTube Zap link](https://zapier.com/app/editor/94806575?redirect=true)

I almost always want to watch anything that comes on either of these YouTube channels, but I don't really watch YouTube, so having these videos land in my Todoist inbox is the best way to make sure I watch them. If I don't want to, I just mark it as done.

## Saved Slack messages to Todoist

[Zap Link](https://zapier.com/shared/2f1cb919caf08d93ecc13727a61ddfb050eac749)

_But Emilie, you can send Slack messages to Todoist with the Todoist Slack integration_. Yes, but this is easier. 

I often times need to action a Slack message- respond in depth or do something- but don't want it to get lost to the Slackbot reminder ether (especially likely if it's not my work Slack). These help keep it front and center. 

## Notify channel when new Slack channels are created

[Zap Link](https://zapier.com/shared/7c8476b28a9e651f98cdf8cef113e101db35d3be)

I use this one in [Locally Optimistic](https://locallyoptimistic.com) to help keep a pulse on Slack channels being created by members! 

## Tweet new RSS feed items

[Zap Link](https://zapier.com/shared/960da497365b58fd81c71f18f3a748c4f690970e)

Also for Locally Optimistic, we are a small team of admins and this helps make sure we _at_ _least_ get one tweet about every blog post out there. 