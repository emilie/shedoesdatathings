---
title: Managing my dotfiles with chezmoi
date: 2020-04-27T04:00:00+00:00
tags: []
photo: ''

---
A couple of weeks ago, I had this horrible hiccup where in trying to manage my python dependencies (aka trying to do the impossible), I deleted vim and all my dotfiles. 

After some panic, I got into a working state enough to work.But there was also a chance here to set things up correctly, better, and more effectively!

When [Kevin](http://lawver.net/) shared about [chezmoi](https://www.chezmoi.io/docs/quick-start/) in [techSAV](https://techsav.co/) Slack, I figured I'd give it a go. 

## Installing

There are multiple ways to [install chezmoi](https://www.chezmoi.io/docs/install/), but I decided to go with homebrew. 

    brew install twpayne/taps/chezmoi

## Getting Started

I followed the [quick start guide](https://www.chezmoi.io/docs/quick-start/#start-using-chezmoi-on-your-current-machine) from their docs with one caveat: A couple of weeks ago, I reimaged my harddrive because this computer had so much _junk_ on it. Despite this (my personal machine) being a 2014 Macbook Air, it is running Catalina 10.15.4, and my terminal is using zsh. 

Just for ease of getting started, I wanted to see what it'd be like to get up and running. I had on my radar [updating my zsh theme to Dracula](https://draculatheme.com/zsh/). After installing, I really simply had to update my `~/.zshrc` file for the `ZSH_THEME` variable to lead to dracula. 

I opened my `~/.zshrc` with `chezmoi edit ~/.zshrc`, and made the change I was interested in.   
  
When I returned to my terminal, I ran `chezmoi diff`. This was the result:

    emilie@Emilies-MacBook-Air ~ % chezmoi diff
    install -m 644 /dev/null /Users/emilie/.zshrc
    --- a/Users/emilie/.zshrc
    +++ b/Users/emilie/.zshrc
    @@ -83,7 +83,7 @@
     # alias ohmyzsh="mate ~/.oh-my-zsh"
    -ZSH_THEME="robbyrussell"
    +ZSH_THEME="dracula"
     TERM=xterm-256color

Now I've made the changes I wanted to make, and chezmoi can see that I made the changes, but I haven't actually committed the changes to my dotfiles repo. 

I applied the changes with `chezmoi -v apply` which updated the chezmoi version of the files, the ones stored in `~/.local/share/chezmoi/dot_zshrc.`

Pleased with my changes I wanted to commit them. By running `chezmoi cd`, I was able to navigate right to the correct directory (without memorizing it). Then I went through a normal Git workflow. 

## Thoughts

It was super easy to get started and I can see how it'd definitely have saved my booty two weeks ago. Chezmoi strikes me as easy to maintain too. There's even auto commit functionality mentioned in the [How To Guide](https://www.chezmoi.io/docs/how-to/). I plan on spending some time setting it up on my other machine and moving through the How To guide so I can take more advantage of the features. 

Maybe I'll even be able to get my two computers working on the same flow soon. I can imagine that'd be a real boost to my productivity. 