---
title: How to Align and Inspire a Remote Team with Joe Martin, Matthew Volm, and Kevin
  Shively
date: 2020-04-22T04:00:00+00:00
tags: []
photo: ''
include_toc: false
---
Earlier this spring, I got to join a discussion with Joe Martin of CloudApp and Matthew Volm of Ally, hosted by Kevin Shively of Ally, on remote work. Especially in this world where we're all suddenly remote, we talked through a ton of really specific tactical tips that can be useful if you're struggling with remote work.

A couple of cool highlights:

* Phases of remote transition
* Connecting with your colleagues as real-people without face-to-face
* Tactical suggestions for working more asynchronously
* a shoutout to the [GitLab handbook](about.gitlab.com/handbook)
* OKRs

<iframe width="560" height="315" src="https://www.youtube.com/embed/N-KkTIPA_4E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Thanks Ally for hosting!

* [Recap blog post](https://www.ally.io/blog/webinar-recap-how-to-align-and-inspire-a-remote-team)
* [Kevin Shively](https://twitter.com/KevinSaysThings)
* [Joe Martin](https://twitter.com/joeDmarti)
* [Matthew Volm](https://twitter.com/heympv)
* [Ally on Twitter](https://twitter.com/gotoally)
* [CloudApp on Twitter](https://twitter.com/CloudApp)