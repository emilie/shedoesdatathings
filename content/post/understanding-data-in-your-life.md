---
date: 2019-04-03 04:00:00 +0000
tags: ["conferences", "geekend"]
photo: ''
title: Understanding Data in Your Life
include_toc: false
---
I will be speaking at [Geekend](http://www.geek-end.com/) at the end of the month. My talk is titled "Understanding Data in Your Life."

The description is:

> Data privacy is only the newest frontier for life in the technological age. In this presentation, data analyst Emilie Schario will talk about the many ways we’re interacting with data in our day-to-day lives. Emilie will start by describing the state of online web tracking and data collection. She will share best practices to be aware of and small changes you can make to be more secure online. Finally, she will talk about the ways we can use the data in our lives for our own benefit. From Fitbits to RescueTime, data collection can be a force for good. We will use Emilie’s health and wellness data as a demonstration throughout the presentation.

[Session Details are available on the Geekend Website](http://www.geek-end.com/sessions/understanding-data/).

General Tickets are still available online!

If you have things you'd like to get out of this session, let me know and I'll be sure it gets included!