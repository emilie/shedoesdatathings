---
title: Another article on working from home in the era of COVID-19
date: 2020-03-29T04:00:00+00:00
tags: []
photo: "/uploads/2020-03-29-emilie-home-office.jpeg"

---
With a couple of years of full-time remote work under my belt, I'm going to add to the series of posts on remote work with my own tools and tips.

## My Top 3

1. Be intentional about getting out of the house. You may be working from home, but that doesn’t mean you’re not leaving your house. Walk your dog or just go for a walk. Get the mail from the mailbox every day. Don’t become trapped in your house.
2. Try setting up a virtual coffee chat with someone for 30 minutes every day. Or a virtual lunch. Zoom is free for 40 minute long calls. Just because you’re home, doesn’t mean you can’t socialize! Don’t know who to set up with, here’s my cal busy/free: [https://calendar.google.com/calendar?cid=ZXNjaGFyaW9AZ2l0bGFiLmNvbQ](https://calendar.google.com/calendar?cid=ZXNjaGFyaW9AZ2l0bGFiLmNvbQ "https://calendar.google.com/calendar?cid=ZXNjaGFyaW9AZ2l0bGFiLmNvbQ") Feel free to send a 30 minute invite to [`emilie@gitlab.com`](mailto:emilie@gitlab.com)
3. Repurpose your commuting time into something fun: an extra hour per day? Read a book! Start a new hobby! Do a yoga routine from YouTube! The possibilities are endless!

## Where I think conventional wisdom is wrong

**You do not need to get dressed every day.** If you're excited about working in your pajamas, **work in your pajamas.**

Don't hesitate to rock what Kevin Lawver calls the "Business Mullet": business from the waist-up, comfortable from the waist-down. I love wearing [Fleo Shorts](https://www.fleo.com/) to work everyday. I would _never_ wear them in-person, but they're so very comfortable.

My work is better because I'm not stressing about what to wear and I'm not confined in something uncomfortable. Wear what makes you feel good.

![](/uploads/2020-03-29-emilie-home-office-2.jpeg)

This is what my home office looks like around lunch time on any given day. Here are some details on what is going on here:

#### Chair

I use a Soul Seat.

#### Desk

Autonomous.ai SmartDesk2, not the Premium

#### Monitor

I bought [this Dell monitor](https://www.amazon.com/gp/product/B00PC9HFNY/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) in June 2017 because someone suggested it.

#### Laptop

This is my work Macbook Pro with the Touchbar

#### Laptop Stand

This [mstand one](https://www.amazon.com/Rain-Design-mStand-Laptop-Patented/dp/B01F01DRW6/ref=sr_1_4?dchild=1&keywords=stand+laptop&qid=1585516454&s=electronics&sr=1-4) because it was the best deal on amazon at the time.

#### Keyboard

I use [this one](https://www.daskeyboard.com/p/prime13-mechanical-keyboard/) from daskeyboard.

#### Mouse

I got a track pad in the week between taking this picture and writing this post.

#### Other things

Water bottle and mug both through Simple modern. Notebook from The Bullet Journal. White board from Amazon. Felix Grey glasses.

Don't stress about cord management if cord management doesn't stress you.