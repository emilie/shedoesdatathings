---
title: 'Social Distance Exposed: Building Connections with Remote Coworkers in the
  Era of Social Distancing'
date: 2020-04-16T04:00:00+00:00
tags: []
photo: ''
draft: true

---
<iframe width="560" height="315" src="https://www.youtube.com/embed/mbFAkdbbaIw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Description from the event: Social Distance Exposed will be an experiment on learning and connecting in the digital age during the COVID-19 outbreak. Join us as we navigate fostering connection and learning outside of our bubbles through virtual gathering in real-time. The event will be spearheaded by Emilie Schario from GitLab, the largest all-remote company in the world. Small group activities and discussions will cover: finding solace in uncertainty, fostering connections virtually, social justice during a pandemic, how to help in your community, and much more. Since social distancing is cutting down on Sunday brunch, use this gathering as an opportunity to grab your quarantine buddy, roommates, and have a brunch at home as you participate!

Eventbrite: https://www.eventbrite.com/e/social-distance-exposed-tickets-100078678020#
Interest form: https://docs.google.com/forms/d/e/1FAIpQLSd3nuQVTtKla394v2b1zXeIBXbl_1DviBfOqSnex10pnQPLlQ/viewform
Issue: https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/2178
