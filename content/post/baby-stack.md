---
title: Baby Stack
date: 2021-03-30T04:00:00.000+00:00
tags: []
photo: "/uploads/nursery_0641.jpeg"

---
At the end of my pregnancy, I found [Caitlin's Baby Stack](https://caitlinhudon.github.io/baby_stack/) to be especially useful. As I've spoken to other impending moms-to-be, I've pointed folks to it again and again. I thought I'd add to the communal knowledge.

## Pregnancy

We found out we were pregnant right as the lockdown was beginning. What a weird year 2020 was.

### Books

It comes as no surprise to anyone who knows me that books are the first thing on my list. There's a lot of info out there, but these are the four things I read that I recommend.

* [Expecting Better](https://www.penguinrandomhouse.com/books/310896/expecting-better-by-emily-oster/) by Emily Oster - I read this as soon as I found out I was pregnant, and it helped me decide where my boundaries in pregnancy were.
* [Cribsheet](https://www.penguinrandomhouse.com/books/572658/cribsheet-by-emily-oster/) by Emily Oster - I waited until RJ was born to read this.
* [All Joy and No Fun](http://jennifersenior.net/the-book) by Jennifer Senior - The first year of marriage after having a child is often considered the hardest year of marriage, and this book helped me prepare and wrap my head around why: it's the long game.
* [Bringing up Bébé](https://pameladruckerman.com/bringing-up-bebe/) by Pamela Druckerman - This aligned with how I thought about most baby things _anyway_, so I enjoyed the read.

### Courses

My husband and I took all of these courses together, and I'm glad we did. My doctor also had a 1-hour overview of labor with one of the birth nurses she regularly works with. That's specific to her, though.

* ["Will I Ever Sleep Again?"](https://takingcarababies.com/newborn-class/) Newborn Sleep class from Taking Cara Babies - MUST TAKE. This is the gift I'm giving to everyone I know who is pregnant from now on.
* [Virtual Lamaze Childbirth Classes](https://mamasandtatas.com/bookings/) from Kiana Ayers of Mamas and Tatas - If it wasn't a pandemic world, I probably would have done this in-person, but Kiana offers these classes online and they're wonderful.
* [Breastfeeding Classes](https://mamasandtatas.com/events/) from Kiana Ayers of Mamas and Tatas
* Birth Plan prep with my [Doula Katie Shuler](https://www.doulasofcolumbus.com/katie-shuler-cd-dona) - Katie was great. My birth plan was pretty chill and included things like "heplock > IV" and "walking monitors plz".

### Wardrobe

I still worked from home and with COVID I rarely left home, so my wardrobe was very small.

* I lived in [this pair of leggings](https://www.amazon.com/gp/product/B06Y6CXM1H/) from Amazon.
* I wore [this maternity suit](https://www.amazon.com/gp/product/B07QMR8M5R/) the one time I went to a pool while pregnant.
* I bought [these maternity bras](https://www.thirdlove.com/collections/nursing-bras) from ThirdLove.
* I got [these tops](https://www.amazon.com/gp/product/B085NDZ3MD/) when my belly didn't fit my tech t-shirts anymore.
* STITCH FIX!! I got two maternity StitchFix boxes that gave me the maternity clothes that I felt best in, including two or three dresses, and two nicer tops.

Even though I was always home, I felt much better when I got dressed, especially when I already felt so pooped.

### Other Pregnancy Items

* I used this [U-Shaped Pregnancy Pillow](https://www.amazon.com/gp/product/B07J5SMJQJ/) when I slept, but I also really didn't sleep well, especially at the beginning of my pregnancy. I am a stomach sleeper, so this helped, but nothing solves for not being able to sleep in the position you've slept in your entire life.
* I was so sick my whole pregnancy. These [Stomach Settle Drops](https://www.amazon.com/UpSpring-Stomach-Settle-Bloating-Sickness/dp/B074MJCC1H/) helped comfort me after the fact but were not preventative.
* Smoothie-flavored Tums > Regular Tums. At the end of my pregnancy, I was consuming over 10 per day and the smoothie-flavored ones made me feel like I was at least getting a treat.

## Hospital Bag

I followed [this hospital bag guide](https://www.babylist.com/hello-baby/what-to-pack-in-your-hospital-bag) from Babylist and [this one from BabyCenter](https://www.babycenter.com/pregnancy/your-body/packing-for-the-hospital-or-birth-center_185). Though, I forgot to pack myself a going-home outfit. It was fine. I did follow Caitlin's advice and buy my own hospital gown, but I wish I had gotten two. I vomited during delivery and couldn't wear it anymore.

## Newborn Stage + Maternity Leave

I took 8 weeks off work. The first two weeks coincided with Casey's Holiday leave from work (so not using his paternity leave). This worked out nicely.

Weeks 9-12, I went back to work part-time. Since RJ needed to be dropped off at daycare a commute was introduced to my schedule for the first time. This was a very welcome change.

## Apps

Holy batman does this baby have a lot of apps.

* Babylist - This helped me manage my registry and what we needed pre-baby.
* [Huckleberry](https://huckleberrycare.com/) - This helped track things like feeding, diapers, naps, and medicine. It was much easier to answer the doctor's questions when I had everything written down.
* [Snoo](https://www.happiestbaby.com/products/snoo-smart-bassinet) - This is the app that works with the bassinet. I love it because it allows me to proactively move up or down a level based on how I'm reading RJ (instead of just the app picking that up). I also like being able to log his night sleep for visuals.
* [Tinybeans](https://apps.apple.com/us/app/tinybeans-baby-family-album/id521633042) - We share photos of RJ with family members via Tinybeans. We don't allow photos of him on Facebook.
* [Pamper's Club](https://www.pampers.com/en-us/rewards) - If you're going to buy diapers, why not get your money back with it? We've gotten $7 back since RJ was born, which isn't a lot of money, but $7 over 18 years with compound interest is $30. If we could do this for lots of $7 increments, we can make a difference. Currently, I transfer this money into a special savings account with the intention of moving it to a 529 plan when we open it (hopefully next month).

## General Gear

### Diaper Bag

We were gifted the [Diaper Bag Backpack from Tactical Baby Gear](https://tacticalbabygear.com/collections/backpacks/products/tbg-daypack-3-0?variant=38577205326). At first, we were kinda _meh_ about it, but it's all-black and the convenience of it being a backpack is nice. It's fine.

### Stroller + Car Seat

We were gifted a hand-me-down [Bugaboo Chameleon 3](https://www.bugaboo.com/us-en/strollers/bugaboo-cameleon-3/bugaboo-cameleon-3-PM00044.html). (Thanks Scott!) This stroller is awesome. We use the bassinet attachment all the time for walks and any time we need to take RJ in and out frequently (church).

After being gifted the stroller, I worked backward to find a car seat that would click right into it. We landed on the [Nuna PIPA Lite Car Seat](https://strolleria.com/products/nuna-pipa-lite-infant-car-seat?variant=16176452599882&msclkid=556aad1c7e791ae7dcf0cd7f6eb07b47&utm_source=bing&utm_medium=cpc&utm_campaign=CP%20%7C%20MF%20%7C%20BNG%20%7C%20SHOP%20%7C%20NBR%20%7C%20CP%20-%20PLA%20-%20BR%20-%20Nuna%20-%20ISO&utm_term=4576648433344466&utm_content=BR%20-%20Nuna). We have a base in each car (mine + C's) and then also a base in NJ. This seat feels nice. The leather handle feels... quality.

Between COVID and the fact that it rains a lot here, we also invested in [the Nuna Rain Cover](https://strolleria.com/products/nuna-pipa-series-rain-cover?_pos=2&_psq=rain%20cover%20nuna%20&_ss=e&_v=1.0). Worthwhile.

![](/uploads/nursery_0641.jpeg)

### At Home

We went with [Delta Children](https://deltachildren.com/) for the furniture for RJ's room. We have a crib, dresser, changing table, and an adorable [bookcase](https://deltachildren.com/collections/storage-decor/products/serta-happy-home-storage-bookcase).

We have the [Delta Children Emerson Glider](https://deltachildren.com/products/emerson-nursery-glider-swivel-rocker-chair?variant=37941650817224) + Footstool in Dove Grey.

![](/uploads/nursery_0642.jpeg)

We also have a [Snoo](https://www.happiestbaby.com/products/snoo-smart-bassinet). I would recommend it to anyone. RJ started sleeping through the night at 7 weeks old.

Downstairs, we have a Pack 'n Play that has a changing table attachment. This has been clutch so that we don't have to go upstairs to change the little man all the time. This has been so nice, we're considering adding a pack and play to my office for when he's hanging out in there with me.![](/uploads/nursery_0656.jpeg)

Having a baby has been the most wonderful blessing of my life. I love and adore my little man. If you're reading this because you're about to have your own, congratulations! I'm excited for you!