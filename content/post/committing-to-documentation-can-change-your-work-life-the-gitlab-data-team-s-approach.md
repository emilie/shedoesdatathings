---
title: 'Committing To Documentation Can Change Your (Work) Life: The GitLab Data Team''s
  Approach'
date: 2019-05-14T04:00:00+00:00
tags: ["speaking","dbt meetup","conference notes"]
photo: "/uploads/speaking_at_dbt_meetup.jpeg"
include_toc: false
---
While in San Francisco last month, I had the honor of presenting at the [dbt Community meetup](https://www.eventbrite.com/e/dbt-community-meetup-data-council-pregame-at-envoy-tickets-56208302546#). My talk was titled **"Committing To Documentation Can Change Your (Work) Life: The GitLab Data Team's Approach"**.

The abstract:

> When we talk about documentation there is an inevitable groan. In this talk, Emilie Schario of GitLab will share how the team's commitment to documentation has supported headcount 5x-ing in less than a year. She'll share three principles you can apply to documentation that can help set your team up for success. Emilie was the first Data Analyst at GitLab and has a built a career around being the first data analyst at growth-oriented startups. She is currently the Data Engineer specializing in Analytics. She is an Army Wife and Princeton University and Venture for America alumna. She currently lives in Savannah, GA.

The GitLab data team is a phenomenal place to work. Our commitment to transparency means all our work is available and public. Here are the links I love to highlight:

* our primary page in the GitLab handbook
* our dbt docs
* our project (repository)

![](/uploads/speaking_at_dbt_meetup.jpeg)

In a recent blog post [4 Examples of the Power of Open Source Analytics](https://about.gitlab.com/2019/04/15/open-source-analytics/), my colleague Staff Data Engineer, Architecture [Taylor Murphy](https://twitter.com/tayloramurphy) wrote "the actual code that you use for analytics isn't your company's competitive advantage." While I didn't realize just how much, working at GitLab has 100% convinced me this is true. GIGO (Garbage in, Garbage out) has a new meaning when you know that retention(whether is financial or product) is all the same analyses, but the quality of the data input is more important than the quality of the code on the page.

The key take way from this talk are our three principles about documentation:

* Documenting (and testing) is part of developing, not an after thought
* Document early and often
* Everyone can contribute

Below are the slides from my presentation. Unfortunately, the talk was no recorded, but I'd be happy to present it again soon. I welcome any thoughts or feedback via [Twitter](twitter.com/emilieschario) or [email](mailto:emilie@gitlab.com).

![](/uploads/speakers_dbt_meetup.JPG)

There were two other presentations that night- how [Envoy](https://envoy.com/) (our wonderful hosts!) wrangled their dbt project and what is coming next for dbt. I am most excited for warn & error test failures in 0.14.0.

<center><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSVI4HAKg_1ymmDPE8QZdzN-IOCPxHjaw-iJbyPVfYhjGzW0STWPZTjnRO9MRq7iVWYQkTgPQNOLnql/embed?start=false&loop=true&delayms=3000" frameborder="0" width="562" height="350" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></center>

P.S. [Obligatory mention that my team is hiring](https://about.gitlab.com/jobs/apply/).