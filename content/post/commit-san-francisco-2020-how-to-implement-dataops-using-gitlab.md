---
title: 'Commit San Francisco 2020: How to Implement DataOps using GitLab'
date: 2020-02-10T05:00:00+00:00
tags: [dataops," data"]
photo: "/uploads/2020-01-14-gitlab-commit-sf.jpg"
include_toc: false
---
DataOps is an emerging practice that applies the principles of DevOps to the field of data- data analytics, data engineering, and data science. But, how do we get data analysts and data scientists working like data engineers? In this talk by GitLab's Internal Strategy Consultant Emilie Schario,  you’ll walk away with three changes you can implement when you get back to your team to help level up your data org and help them start implementing DataOps. You’ll learn exactly where to get started with DataOps from some common language and concepts to setting up a DataOps workflow using GitLab.

Slide deck: https://drive.google.com/file/d/16-NYw1owSzONRXgpqQvQ7Yrc1Y75zd8A/view?usp=sharing

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/GSEwkL5ZRNs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>