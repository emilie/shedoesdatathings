---
title: Having role models who look like you is important
date: 2019-08-23T04:00:00+00:00
tags: []
photo: "/uploads/interview_with_tfir_at_contribute.jpg"
include_toc: false
---
I got to speak with Swapnil of [TFIR](https://www.tfir.io/) while at GitLab Contribute earlier this year, and this takeaway from our interview is spot on.

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/6IvocbUI6lA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

**It is important to be able to look around the room and have people who look like you.**