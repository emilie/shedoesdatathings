---
title: Analyzing your GoodReads data with Meltano to better understand your Reading
  Habits
date: 2020-01-20T05:00:00+00:00
tags: []
photo: ''
draft: true

---
Oh, that time of year when we reflect on our habits and plan for the thousands of ways we're going to get better. I've always loved [my year in review from GoodReads](https://www.goodreads.com/user/year_in_books/2019/37809322), but they're notably light on _actually_ useful information.

![](/uploads/goodreads_2019.png)

The thing about this Year in Review is that there's really not much useful information here. There is:

* Total pages read
* Total books read
* Shortest book length
* Longest book length
* Average book length
* Most popular book
* Least popular book
* My average rating
* Highest rated book that I read

These are great. They're numbers that make me _feel good_ and give me the _warm and fuzzies_ about _how great a reader I am_ but these aren't particularly useful data points, if we're being honest.

Like all things, if you want to do a real data analysis, you need to gather the data yourself.

Inspired by [Claus's NFL dbt repo](https://calogica.com/dbt/2019/12/16/nfl-dbt-repo.html), I thought I'd go ahead and create something similar: an environment in which I would get the raw data on the books I'd read and be able to get answers to some of my own questions.

I'm a big fan of [Meltano](meltano.com), and I thought this was the perfect opportunity to make it easier for more people to use.

## Start your Meltano project

At the risk of this blog post turning into a tutorial, I'm just going to link to a lot of places where you can help yourself, but I'd also give a brief overview of how to get the pieces to fit together.

I recommend [installing Meltano](https://meltano.com/docs/installation.html#local-installation) into a virtual environment using pip. The easy thing to do here if you're just looking for commands (assuming you've got python, etc installed).

    mkdir ~/.venv #create a new folder called .venv in your root directory
    python -m venv ~/.venv/meltano #create virtual environment for your to install meltano into
    source ~/.venv/meltano/bin/activate #activate virtual environment
    pip install meltano #install meltano inside your virtual environment
    meltano --version # should output your version

I'm writing this on version `1.19.2`

Great. Now you've got Meltano installed in a virtual environment.

I'm going to assume we're going to create your Meltano project called `reading_analysis` in the root directory.

Run `meltano init goodreads_analysis`. This initiates your project.

Meltano is organized into a logical structure with how we organize data projects:

* Extract - pull data from its source
* Load - put data into where it will be centralized
* Transform - make any adjustments/cleansing to the data using dbt
* Model - the mapping layer between the database and the analysis step which helps Meltano know how to write queries for you
* Analyze - [data analysis](https://about.gitlab.com/blog/2019/11/04/three-levels-data-analysis/)

Meltano does other things, but we're not using them so I won't mention them here.

## Extract

I did not want to write a Singer tap to get my goodreads data, so instead, I just pulled the info I wanted. I used requests to scrape the page, beautiful soup to parse it, and output a csv.

I wrote [this](https://gitlab.com/emilie/reading_analysis/blob/master/get_data.py) for me, but you're welcome to use it too.

* You'll need to adjust the `profile_info` variable to your profile.
* This only pulls the first page of data. This is to control the requests. You should adjust it to run for all of your data.

The resulting csv needs to be stored in `~/reading_analysis/extract`. Cool, with our nifty CSV in place, we can get to the fun parts.

We want to use `tap-csv` to get our data into postgres. tap-csv is hidden by-default in Meltano, so the first thing we need to do is un-hide it.

Assuming you're in `~/reading_analysis/` run `meltano add extractor tap-csv`. Then run `meltano ui` and load up `http://localhost:5000/` in your browser and you should see the beautiful Meltano UI.

![](/uploads/meltano_ui.jpg)

Isn't it pretty?!

Before we configure tap-csv, there is one more thing we need to do: we need to create a file that tells our tap csv just which files it should expect and what makes those files special. I used the same naming convention that the Meltano tutorial did- `files_def.json`

My file is simple (also [here](https://gitlab.com/emilie/reading_analysis/blob/master/extract/files_def.json))-

    [
      { "entity": "books", "file": "extract/books_read.csv", "keys": ["row_number"] }
    ]

Now, we're ready to return to the UI where we can configure tap-csv to look for our `files_def.json`

#### Just a quick note

I also did an analysis on my reading pace vs goal, which is why my linked files definition file has another csv file, called `reading_goals.csv` which looks like this:

    row_number,calendar_year,reading_goal
    0,2020,38
    1,2019,32
    2,2018,36
    3,2017,30
    4,2016,26
    5,2015,7

This analysis was a lot more fun to prep.  
The code examples in [reading_analysis](https://gitlab.com/emilie/reading_analysis/) reflect the whole kit and caboodle.

## Load

Keeping it simple with postgres and target-postgres. Assuming you have [postgres installed](https://www.postgresql.org/download/macosx/) and the server turned on, you should be able to configure tap-postgres by just updating the credentials!

## Transform

For our purposes, our transformations are pretty simple:

* we're updating column names
* we're casting data types appropriately, e.g. numbers and dates

To get this transformation, we need to put the `books_read.sql` file under `transform/models`. (Also [here](https://gitlab.com/emilie/reading_analysis/blob/master/transform/models/books_read.sql))

    with source as (
    
      SELECT *
      FROM tap_csv.books
    
    ), renamed as (
    
      SELECT "date_read_value"::date as date_read_value,
             date_trunc('month', "date_read_value"::date)::date as date_read_month,
             date_trunc('year', "date_read_value"::date)::date as date_read_year,
             "date_started_value"::date as date_started_value,
             "field_author"::varchar as book_author,
             "field_num_pages"::float as book_number_pages,
             "field_title"::varchar as book_title,
             "row_number"::int as row_id
      FROM source
    )
    
    SELECT *
    FROM renamed

If we were doing a dbt project here, we'd need to make other changes to the configuration, but Meltano handles all of that for you!

Now, go back to the UI and create that first pipeline!

![](/uploads/meltano_pipeline.jpg)

#### If you're doing it all...

Try updating your `transform/packages.yml` with

    packages:
    - git: https://gitlab.com/emilie/reading_analysis.git
      revision: master

instead of what's above!

## Model

Model files are the layer that tells Meltano how to write queries against the postgres database. Meltano calls these `.m5o` files, and there are two kinds: tables and topics. Tables map 1:1 with dbt models. Topics are how tables are related to each other. (Read more on the [Meltano Model.](https://www.meltano.com/docs/architecture.html#meltano-model)) Model files go in the `/model` directory and Meltano does a good job of picking up on them.

We need two files to get started ([see mine](https://gitlab.com/emilie/reading_analysis/tree/master/model)):

* `books_read.table.m50`
* `reading_analysis.topic.m5o`

## Analyze

And.. tada!

![](/uploads/meltano_analysis.png)

We can now use the drag and drop interface to start asking better questions of our data! For example, I didn't finish _any_ books on April.

Now that the bones are there, there's so much more we can do.

If you want to use my dashboards as a starting point, you can borrow what's in my [analyze](https://gitlab.com/emilie/reading_analysis/tree/master/analyze) folder.

## Eventually...

There is so much you can do with this data, and so much you can enrich about it.  I'd like to be able to enrich this data with author genders, genre of book, etc. This is just the start.