---
title: 1 year at GitLab
date: 2019-06-22T04:00:00+00:00
tags: [gitlab, " career"]
photo: ''

---
![](/uploads/speaking_at_dbt_meetup.jpeg)

This month, I celebrated my one year anniversary at GitLab. It's crazy to think that last June I was stepping into this company that has been so impactful for me personally and professionally. In some ways, I am shocked that it has _already_ been a year; in others, I feel like I just started. It has been an incredible opportunity that I'd like to think I've (mostly) seized. I know I still have a lot to learn, grow, and develop, but I feel like GitLab is the place I'm meant to be doing that right now.

## Vacation and flexibility

It seems fitting that the actual day I crossed the 1-year mark- June 5 (I started the day after GitHub was acquired!)- I was away on a 2-week vacation. I've taken more time off while at GitLab than at any previous company. The unlimited vacation policy is not just "unlimited vacation", but a real suggestion to take time off and recharge whenever you need it. I took a week off in September last year (vacation to Key West), a week off in November (to get married), just now two weeks off in June (vacation, again), multiple days off to attend conferences (FinCon, MIC Orlando, PyTN, Women in Analytics, DataEngConf SF - I'm finishing writing this while on a plane to KubeCon Shanghai!), and a miscellaneous collection of days sprinkled throughout.

Some may see that and gasp at my 5+ weeks of vacation in the last 12 months, but I don't think anyone on my leadership team would do that. I'm a better employee because of this time away from work. I do feel that I'm measured on the quality of work I've delivered, not on the number of days my butt was in my office chair. (A lot of companies _say_ this, but I feel this is _actually true_.) As long as I keep delivering the high quality work I've been doing, I don't see anyone gasping at my many, many vacation days.

The freedom to take time off whenever I need to is definitely part of what makes me feel like I'm doing my best work yet. I am not putting off doctor's appointments because of the required day off from work (I am, though, still putting off dentist appointments but for different reasons). I am proactively taking care of me and doing what is necessary to keep me in shape.

Earlier this year, I switched gyms to work with a new trainer. I now workout at 8 AM which, after years of training at 5 AM, feels like early afternoon. I start my work day after my morning routine, then go to the gym, and then get back to work. I am in a better state of mind when I get my workout in, not just once but consistently. For me, there is an empowering feeling that comes from feeling strong- whether that's running a half marathon with no training (yes, I did that), deadlifting 245 for reps, or pushing just a little harder on the Assault Bike this week than last; that feeling is something that translates into how I work. Without the flexibility, I get from GitLab to work wherever and whenever is _best for me_, I certainly wouldn't be working with _this trainer_ at _this gym_ and making the progress I'm making right now.

I highlight vacation and flexibility first as I reflect on the last year because **my work is just a part of who I am and what I do**; living my life is most important. We should stop calling it "work/life balance" and start calling it "life/work balance." I feel really good about the cycle of life/work balance I've developed while at GitLab. While it hasn't been all ponies and rainbows, the experience has been overwhelming positive.

## Leadership

My entire career has been in startups, thanks in large part to my time in [Venture for America](http://ventureforamerica.org/). What separates GitLab from all of those other organizations, including some that were similarly sized, is the level of leadership that I see throughout the organization. Learning doesn't only happen when transferring domain-specific knowledge from one person to another; we (I?) learn through observation. GitLab has been incredibly generous giving me the opportunity to see how its senior leaders build their teams. As I work to level up and grow in my own career, I look to the example of these leaders- how they are interacting with each other, across the organization; managing conflict, miscommunications, or competing interests; negotiating and balancing priorities with the rest of the organization. I read "Getting to Yes" this spring (thanks to the [Not So Standard Deviations](http://nssdeviations.com) podcast for the book club!), and I was so impressed to be able to think of experiences at GitLab where I saw principled negotiation at play.

Later this summer, I'll be participating in the [CEO Shadow program](https://about.gitlab.com/handbook/ceo/shadow/), where I'll spend two weeks learning directly from Sid. Much of the feedback from other shadows to date has highlighted the power of understanding Sid's interactions with his direct reports. I know that there is still a lot to learn here, but this has been truly transformative so far.

## Ego & Collaboration

Being part of a real team for the first time has forced me to more regularly disagree and commit. For example, I don't agree with everything in the [GitLab Data SQL style guide](https://about.gitlab.com/handbook/business-ops/data-team/sql-style-guide), but I do commit to using it every day. My successes are not just mine anymore. They're my team's. My ways of thinking are not just mine anymore- they're also my team's. My ideas are constantly being shaped by the feedback I'm getting, challenged by differing opinions, and the way I see people working.

We were three on the Data Team when I started. Today, we're seven (3 Data Engineers, 3 Data Analysts, 1 Intern). Next week, two more analysts start. (And we're hiring!) The team is growing, and I need to get better at collaborating, especially collaborating asynchronously.

In the past year, I have had to manage expectations better when there are more eyes on me and my projects, more publicly admit mistakes, liaise with people two or three levels higher than me in the organization, and more. These things have forced me to recognize how much I let my ego selfishly dictate my actions. I'll admit that keeping my ego in check is not the easiest for me, but it is something I've been actively working on, and the last year at work has really been pushing me to get better. Being part of a team requires me to do better; it is not easy, but I am working on it. (Big thanks to [Danielle Morrill](https://twitter.com/DanielleMorrill), General Manager of [Meltano](meltano.com), who gave me feedback on how my ego was shaping a thought-process. That feedback was incredibly powerful for me.)

## Iteration and Transparency

The GitLab way of the [MVC](https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc) makes people nervous. Add that to my team's commitment to [Open Source Analytics](https://about.gitlab.com/2019/04/15/open-source-analytics/), and you've created some folks' worst career nightmares. My time at GitLab, though, has convinced me they're all wrong. I think of it like this: yes, working in public and shipping things that work but aren't fully fleshed out is like walking around outside in your underwear, but it's the middle of the night and everyone is asleep anyway. tl;dr Nobody is judging (but if people see you, they _will_ look).

The benefits that come from quick iterations ("Is this what you actually want? Does this analysis/report/project address the problem you're looking to solve?") and transparency (sharing our code with the community!) are not just for me and my team but also to the entire data ecosystem.

![](/uploads/claire_asks_for_a_gitlab_example.jpg "Claire, community manager for dbt slack, asking for an example of our team using date-spining for reference.")

([Claire](https://twitter.com/ClaireDoesData), community manager for [dbt](https://getdbt.com), asking for an example of our team using date-spining for reference.)

Being asked for an example of _hey, where is the GitLab team doing this in production_ and then being able to link to _right here_ is incredibly powerful not just for us (we have these sorts of questions internally, as well, and more frequently as the team is growing), but for the any data folks who stumble upon our work. By contributing to the data community, we better the community; we learn and benefit from a better community. We create a mutual cycle of growth.

## Miscellaneous Other Lessons

* Hiring is really hard.
* Prioritizing is really hard.
* ESTIMATING IS REALLY HARD AND I AM REALLY BAD AT IT.
* Sleep with a notebook next to your bed because you will solve work problems in your dreams.
* You will never get what you don't ask for, so ask because getting a "no" is not that bad.
* I don't know the balance between ambition and apathy.
* Celebrate folks whenever you can.

## What's next?

Last month, I was promoted from Data Analyst to Data Engineer, Analytics (Aside: we can have a long discussion on career ladders and how I moved from one to the other not up the DA ladder some other time- I call this a promotion because it _feels to me like_ a promotion, since it's something I worked toward). I've gone from a role where I was crushing it (technical term) _all the time_ to having responsibilities and taking on projects that put me outside of my comfort zone.

At GitLab, I feel pushed and comfortable (safe to fail?) at the same time. I think it has something to do with being part of a team, where I know even if I can't solve a problem I have brilliant coworkers who can pair with me to do that.

I've grown so much in the last year. I'm grateful for the opportunities I've been provided. The team plans to grow a lot this year, and I look forward to growing with it.

(Picture: Speaking at the SF dbt Meetup in the Spring)