---
title: Every Open Source Contribution Matters
date: 2019-08-31T04:00:00+00:00
tags: ["open source"]
photo: "/uploads/2018-08-23-meltano_demo_day.jpg"
include_toc: false
---
It can often feel like contributing to open source is _a thing_ exclusive to rockstar developers.

I think Open Source is a place to work on the skills I want to work on. When I decided I was going to get my first contribution into [Meltano](Meltano.com "Meltano") earlier this year, I was looking for low-hanging fruit.

In my experience, most open source projects have a label like `Accepting Contributions` or `good first issue`. These are a great way to get your foot in the door!

In last week's Meltano Demo Day, I was excited to be able to mention my own contribution to Meltano ([my FOURTH to the project!!](https://gitlab.com/groups/meltano/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=emilie)) because this is the first time that touched the code base.

Stepping into a new code base is hard. **It's even harder when it's something we might do on a night or weekend and not come back to for a few weeks.** Contributing to Open Source doesn't mean it's your second job. I do it when I see a problem to be solved that I think I can solve, or I have a skill I'm looking on working on. In this light, contributing to Open Source can be a win-win.

In case you were wondering I've also contributed to:

* [dbt core](https://github.com/fishtown-analytics/dbt/pulls?q=is%3Apr+is%3Aclosed+author%3Aemilieschario)
* [dbt-utils](https://github.com/fishtown-analytics/dbt-utils/pulls?q=is%3Apr+is%3Aclosed+author%3Aemilieschario)
* [dbt snowplow package](https://github.com/fishtown-analytics/snowplow/pulls?q=is%3Apr+is%3Aclosed+author%3Aemilieschario)

This doesn't include my work at GitLab!

If you poke around, you'll see that none of my contributions are _remarkable_ but they made something a little better.

If you're looking to start contributing to Open Source, GitLab is hosting a [Hackathon on November 13-14](https://about.gitlab.com/community/hackathon/). It's all remote (like we do all things) and is a great way to dip your toes into Open Source!

Here is last week's Meltano Demo Day. Thanks to the Meltano team for the chance to share my contribution!

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/HCJpwIzxOpQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>